﻿/// <reference path="../libs/knockout/knockout-3.2.0.debug.js" />
/// <reference path="../libs/knockout/knockout.mapping-latest.debug.js" />
/// <reference path="../libs/theme/jquery.js" />
window._originalAlert = window.alert;
window.alert = function (text) {
    var bootStrapAlert = function () {
        if (!$.fn.modal.Constructor)
            return false;
        if ($('#windowAlertModal').length == 1)
            return true;
        $('body').append(
          '<div class="modal fade" id="windowAlertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
            '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                  '<div class="modal-body">' +
                  '<p></p>' +
                  '</div>' +
                  '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-xs btn-default" data-dismiss="modal">Close</button>' +
                  '</div>' +
                '</div>' +
           '</div>' +
         '</div>');
        return true;
    }
    if (bootStrapAlert()) {
        $('#windowAlertModal .modal-body p').html(text);
        $('#windowAlertModal').modal();
    } else {
        console.log('bootstrap was not found');
        window._originalAlert($("<div>" + text + "</div>").text());
    }
}
window._originalConfirm = window.confirm;
window.confirm = function (text, cb) {
    var initTemplate = function () {
        if ($('#windowConfirmModal').length == 1)
            return true;
        $('body').append(
             '<div class="modal fade" id="windowConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
            '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                  '<div class="modal-body">' +
                  '<p></p>' +
                  '</div>' +
                  '<div class="modal-footer">' +
                    '<button class="btn btn-xs btn-danger" data-dismiss="modal" aria-hidden="true">No</button>' +
                    '<button class="btn btn-xs btn-primary" data-dismiss="modal" aria-hidden="true">Yes</button> ' +
                  '</div>' +
                '</div>' +
           '</div>' +
         '</div>'
           );
    }

    var bootStrapConfirm = function () {
        if (!$.fn.modal.Constructor)
            return false;

        $('body').off('click', '#windowConfirmModal .btn-primary');
        $('body').off('click', '#windowConfirmModal .btn-danger');

        function confirm() { cb(true); }
        function deny() { cb(false); }

        $('body').on('click', '#windowConfirmModal .btn-primary', confirm);
        $('body').on('click', '#windowConfirmModal .btn-danger', deny);

        return true;
    }

    initTemplate()
    if (bootStrapConfirm()) {
        $('#windowConfirmModal .modal-body p').html(text);
        $('#windowConfirmModal').modal();
    } else {
        console.log('bootstrap was not found');
        cb(window._originalConfirm($("<div>" + text + "</div>").text()));
    }

}


$(function () {
    var _ws = 0;
    var updateTime = function (fl) {
        _ws = _ws + fl;
        if (_ws > 0) {
            $(".wait-signal").show();
        } else {
            $(".wait-signal").hide();
        }
    };
    $(document).ajaxStart(function (e) {
        updateTime(1);
    });
    $(document).ajaxStop(function () {
        updateTime(-1);
    });

    /* Smooth scrolling para anclas */
    $(document).on('click', 'a.smooth', function (e) {
        e.preventDefault();
        var $link = $(this);
        var anchor = $link.attr('href');
        $('html, body').stop().animate({
            scrollTop: $(anchor).offset().top
        }, 1000);
    });

    $('.nav').on('click mousedown mouseup touchstart touchmove', 'a.has_children', function () {
        if ($(this).next('ul').hasClass('open_t') && !$(this).parents('ul').hasClass('open_t')) {
            $('.open_t').removeClass('open_t');
            return false;
        }
        $('.open_t').not($(this).parents('ul')).removeClass('open_t');
        $(this).next('ul').addClass('open_t');
        return false;
    });
    $(document).on('click', ':not(.has_children, .has_children *)', function () {
        if ($('.open_t').length > 0) {
            $('.open_t').removeClass('open_t');
            $('.open_t').parent().removeClass('open');
            return false;
        }
    });

    // hide #back-top first
    $("#back-top").hide();

    // fade in #back-top

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#back-top').fadeIn();
        } else {
            $('#back-top').fadeOut();
        }
    });

    // scroll body to 0px on click
    $('#back-top a').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });

    $('#status').fadeOut(); // will first fade out the loading animation


    var viewModel = {
        Title: ko.observable(),
        Message: ko.observable()        
    };

    viewModel.SaveMessage = function () {
        if ($("#feedbackform").valid()) {
            //return;
            var jsonData = ko.mapping.toJSON(viewModel);
            $.ajax({
                url: "/Home/SaveMessages",
                type: "POST",
                datatype: "json",
                data: jsonData,
                contentType: "application/json; charset=utf-8;",
                success: function (data) {
                    if (data.Result == "OK") {
                        viewModel.Title("");
                        viewModel.Message("");
                        alert("Thankyou for the feedback");
                    }
                },
                complete: function () {
                }
            });
        }

    };

    $('#promo').on('hidden.bs.modal', function () {
        $('#promo').hide();
        $('#promoframe iframe').attr("src", '');
    });

    viewModel.UpdateVisits = function () {

        var data = {
            URL: window.location.href
        };

        var jsonData = ko.mapping.toJSON(data);
        $.ajax({
            url: "/Home/UpdateVisits",
            type: "POST",
            datatype: "json",
            data: jsonData,
            contentType: "application/json; charset=utf-8;",
            success: function (data) {

            },
            complete: function () {
            }
        });
    };

    ko.applyBindings(viewModel, document.getElementById("feedbackform"));
    $("#feedbackform").validate({});
    viewModel.UpdateVisits();
    //viewModel.GetTopperList();
});

