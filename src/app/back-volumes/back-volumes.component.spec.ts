import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackVolumesComponent } from './back-volumes.component';

describe('BackVolumesComponent', () => {
  let component: BackVolumesComponent;
  let fixture: ComponentFixture<BackVolumesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackVolumesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackVolumesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
