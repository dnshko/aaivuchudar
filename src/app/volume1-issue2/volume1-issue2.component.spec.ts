import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Volume1Issue2Component } from './volume1-issue2.component';

describe('Volume1Issue2Component', () => {
  let component: Volume1Issue2Component;
  let fixture: ComponentFixture<Volume1Issue2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Volume1Issue2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Volume1Issue2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
