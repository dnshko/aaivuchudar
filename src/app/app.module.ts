import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SliderComponent } from './slider/slider.component';
import { FooterComponent } from './footer/footer.component';
import { AboutAsComponent } from './about-as/about-as.component';
import { PageContentComponent } from './page-content/page-content.component';
import { CorrespondentComponent } from './correspondent/correspondent.component';
import { PrincipalComponent } from './principal/principal.component';
import { DirectorComponent } from './director/director.component';
import { ArticlesComponent } from './articles/articles.component';
import { ArticleSubmissionComponent } from './article-submission/article-submission.component';
import { ContactComponent } from './contact/contact.component';
import { BackVolumesComponent } from './back-volumes/back-volumes.component';
import { Volume1Component } from './volume1/volume1.component';
import { Volume2Component } from './volume2/volume2.component';
import { Volume1Issue2Component } from './volume1-issue2/volume1-issue2.component';
import { NotFoundComponentComponent } from './not-found-component/not-found-component.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SliderComponent,
    FooterComponent,
    AboutAsComponent,
    PageContentComponent,
    CorrespondentComponent,
    PrincipalComponent,
    DirectorComponent,
    ArticlesComponent,
    ArticleSubmissionComponent,
    ContactComponent,
    BackVolumesComponent,
    Volume1Component,
    Volume2Component,
    Volume1Issue2Component,
    NotFoundComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
