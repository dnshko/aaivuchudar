import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutAsComponent } from './about-as/about-as.component';
import { CorrespondentComponent } from './correspondent/correspondent.component';
import { PrincipalComponent } from './principal/principal.component';
import { DirectorComponent } from './director/director.component';
import { ArticlesComponent } from './articles/articles.component';
import { ArticleSubmissionComponent } from './article-submission/article-submission.component';
import { ContactComponent } from './contact/contact.component';
import { BackVolumesComponent } from './back-volumes/back-volumes.component';
import { Volume1Component } from './volume1/volume1.component';
import { Volume2Component } from './volume2/volume2.component';
import { Volume1Issue2Component } from './volume1-issue2/volume1-issue2.component';
import { NotFoundComponentComponent } from './not-found-component/not-found-component.component';


const routes: Routes = [
  { path:  'about-as', component:  AboutAsComponent},
  { path:  'Correspondent', component:  CorrespondentComponent},
  { path:  'Principal', component:  PrincipalComponent},
  { path:  'Director', component:  DirectorComponent},
  { path:  'Articles', component:  ArticlesComponent},
  { path:  'ArticleSubmission', component:  ArticleSubmissionComponent},
  { path:  'Contact', component:  ContactComponent},
  { path:  'BackVolumes', component:  BackVolumesComponent},
  { path:  'Volume1', component:  Volume1Component},
  { path:  'Volume2', component:  Volume2Component},
  { path:  'Volume1Issue2', component:  Volume1Issue2Component},
  { path: '', component: AboutAsComponent},
  { path: '404', component: NotFoundComponentComponent },
  { path: '**', redirectTo: '404' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
