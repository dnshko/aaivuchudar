import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleSubmissionComponent } from './article-submission.component';

describe('ArticleSubmissionComponent', () => {
  let component: ArticleSubmissionComponent;
  let fixture: ComponentFixture<ArticleSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
